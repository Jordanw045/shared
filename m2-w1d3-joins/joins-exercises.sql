-- The following queries utilize the "dvdstore" database.

-- 1. All of the films that Nick Stallone has appeared in
--    Rows: 30
SELECT title
FROM film
WHERE film_id IN (
SELECT film_id
FROM film_actor
WHERE actor_id =(
SELECT actor_id
FROM actor
WHERE first_name = 'NICK' AND last_name = 'STALLONE'));

SELECT title
FROM film
INNER JOIN film_actor fa
ON film.film_id = fa.film_id
INNER JOIN actor
ON fa.actor_id = actor.actor_id
WHERE actor.first_name = 'NICK' AND actor.last_name = 'STALLONE';

SELECT title
FROM film
INNER JOIN film_actor fa
ON film.film_id = fa.film_id
INNER JOIN actor
ON fa.actor_id = actor.actor_id
WHERE actor_id disappointed
SELECT actor_id
FROM actor
WHERE first_name = 'NICK' AND last_name = 'STALLONE'));


-- 2. All of the films that Rita Reynolds has appeared in
--    Rows: 20
SELECT title FROM film WHERE film_id IN ( SELECT film_id FROM film_actor WHERE actor_id=( SELECT actor_id FROM actor WHERE first_name='RITA' AND last_name='REYNOLDS'));

-- 3. All of the films that Judy Dean or River Dean have appeared in
--    Rows: 46
SELECT title FROM film WHERE film_id IN ( SELECT film_id FROM film_actor WHERE actor_id IN( SELECT actor_id FROM actor WHERE last_name='DEAN' ));

-- 4. All of the the ‘Documentary’ films
--    Rows: 68
SELECT title FROM film WHERE film_id IN ( SELECT film_id FROM film_category WHERE category_id =( SELECT category_id FROM category WHERE name ='Documentary' ));
-- 5. All of the ‘Comedy’ films
--    Rows: 58
SELECT title FROM film WHERE film_id IN ( SELECT film_id FROM film_category WHERE category_id =( SELECT category_id FROM category WHERE name ='Comedy' ));
-- 6. All of the ‘Children’ films that are rated ‘G’
--    Rows: 10 
SELECT title FROM film WHERE rating ='G' AND film_id IN ( SELECT film_id FROM film_category WHERE category_id =( SELECT category_id FROM category WHERE name ='Children' ));
-- 7. All of the ‘Family’ films that are rated ‘G’ and are less than 2 hours in length
--    Rows: 3
SELECT title FROM film WHERE rating ='G' AND length < 120 AND film_id IN ( SELECT film_id FROM film_category WHERE category_id =( SELECT category_id FROM category WHERE name ='Family' ));
-- 8. All of the films featuring actor Matthew Leigh that are rated ‘G’
--    Rows: 9
SELECT title FROM film WHERE rating ='G' AND film_id IN ( SELECT film_id FROM film_actor WHERE actor_id =( SELECT actor_id FROM actor WHERE first_name ='MATTHEW' AND last_name = 'LEIGH'));

-- 9. All of the ‘Sci-Fi’ films released in 2006
--    Rows: 61
SELECT title FROM film WHERE release_year ='2006' AND film_id IN ( SELECT film_id FROM film_category WHERE category_id =( SELECT category_id FROM category WHERE name ='Sci-Fi'));
-- 10. All of the ‘Action’ films starring Nick Stallone
--     Rows: 2



select f.title
from film f
inner join film_category c on f.film_id = c.film_id
inner join category a on c.category_id = a.category_id
inner join film_actor b on c.film_id = b.film_id
inner join actor e on e.actor_id = b.actor_id
where a.name = 'Action' AND e.last_name = 'STALLONE';

-- 11. The address of all stores, including street address, city, district, and country
--     Rows: 2
select a.address,c.city, co.country, a.district
from store s 
inner join address a on s.address_id=a.address_id
inner join city c on a.city_id = c.city_id
inner join country co on c.country_id = co.country_id
where a.district is NOT NULL;

-- 12. A list of all stores by ID, the store’s street address, and the name of the store’s manager
--     Rows: 2
select s.store_id, a.address,(sf.last_name||', '||sf.first_name) as full_name 
from store s 
inner join address a on s.address_id=a.address_id
inner join staff sf on s.manager_staff_id= sf.staff_id;

-- 13. The first and last name of the top ten customers ranked by number of rentals 
--     Hint: #1 should be “ELEANOR HUNT” with 46 rentals, #10 should have 39 rentals
SELECT (last_name||', '||first_name) as full_name
from customer
inner join rental b on customer_id = b.customer_id
order by full_name desc;


-- 14. The first and last name of the top ten customers ranked by dollars spent 
--     Hint: #1 should be “KARL SEAL” with 221.55 spent, #10 should be “ANA BRADLEY” with 174.66 spent

-- 15. The store ID, street address, total number of rentals, total amount of sales (i.e. payments), and average sale of each store 
--     Hint: Store 1 has 7928 total rentals and Store 2 has 8121 total rentals
SELECT s.store_id,count(p.rental_id),a.address, sum(amount)
from payment p
inner join rental r on p.rental_id = r.rental_id
inner join inventory i on r.inventory_id = i.inventory_id 
inner join store s on i.store_id = s.store_id
inner join address a on s.address_id = a.address_id
group by  s.store_id,p.rental_id,a.address, amount;

SELECT s.store_id, sum(inventory_id)

select s.store_id, a.address, sum(avg(p.amount)
from address a
inner join store s on a.address_id=s.address_id
inner join 


-- 16. The top ten film titles by number of rentals 
--     Hint: #1 should be “BUCKET BROTHERHOOD” with 34 rentals and #10 should have 31 rentals
SELECT f.title,count(f.title) as num
from film f 
inner join inventory i on f.film_id = i.film_id 
inner join rental r on i.inventory_id = r.inventory_id
group by f.title
order by num desc;
LIMIT 10;
-- 17. The top five film categories by number of rentals 
--     Hint: #1 should be “Sports” with 1179 rentals and #5 should be “Family” with 1096 rentals
SELECT f.name,count(f.name) as num
from category f
inner join film_category i on f.category_id = i.category_id 
inner join film g on i.film_id = g.film_id 
inner join inventory r on g.film_id = r.film_id
inner join rental a on r.inventory_id = a.inventory_id
group by f.name
order by num desc
LIMIT 10;
-- 18. The top five Action film titles by number of rentals 
--     Hint: #1 should have 30 rentals and #5 should have 28 rentals
SELECT f.title,count(f.title) as num
from film f 
inner join inventory i on f.film_id = i.film_id 
inner join rental r on i.inventory_id = r.inventory_id
inner join film_category c on f.film_id = c.film_id
inner join category a on c.category_id = a.category_id 
where a.name = 'Action'
group by f.title
order by num desc
LIMIT 5;
-- 19. The top 10 actors ranked by number of rentals of films starring that actor 
--     Hint: #1 should be “GINA DEGENERES” with 753 rentals and #10 should be “SEAN GUINESS” with 599 rentals

-- 20. The top 5 “Comedy” actors ranked by number of rentals of films in the “Comedy” category starring that actor 
--     Hint: #1 should have 87 rentals and #10 should have 72 rentals
