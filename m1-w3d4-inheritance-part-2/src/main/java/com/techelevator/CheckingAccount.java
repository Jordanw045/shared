package com.techelevator;

public class CheckingAccount extends BankAccount {

	public CheckingAccount(DollarAmount balance, String accountNumber,  BankCustomer customer) {
		super(balance, accountNumber, customer);
		// TODO Auto-generated constructor stub
	}

	@Override
	public DollarAmount withdraw(DollarAmount withdraw) {
		DollarAmount penalty = new DollarAmount (1000);
		if (withdraw.isLessThanOrEqualTo(balance)) {
			 super.withdraw(withdraw);
		}
		else if (withdraw.isGreaterThan(balance)) {
			if (((withdraw).plus(penalty)).isGreaterThan(new DollarAmount(10000))) {
				return balance;
			}
			else {
				super.withdraw(withdraw).plus(penalty);
			}
		}	
		return super.getBalance();
	}

	@Override
	public void transfer(BankAccount destinationAccount, DollarAmount transferAmount) {
		DollarAmount penalty = new DollarAmount (1000);
		if(transferAmount.isLessThanOrEqualTo(getBalance())) {
			 super.withdraw(transferAmount);
			 destinationAccount.deposit(transferAmount);
		} 
		else if (transferAmount.isGreaterThan(getBalance())) {
			if ((super.withdraw(transferAmount).plus(penalty)).isLessThan(new DollarAmount(-100))) {
				return;
			}
		}
		else{
			super.withdraw(transferAmount).plus(penalty);
			destinationAccount.deposit(transferAmount);
		}
	}
}
