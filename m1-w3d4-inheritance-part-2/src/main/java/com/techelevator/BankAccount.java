package com.techelevator;

public class BankAccount {
	public BankAccount( DollarAmount balance,String accountNumber, BankCustomer customer) {
		this.balance=balance;
		this.accountNumber= accountNumber;
		this.customer = customer;
		
		// TODO Auto-generated constructor stub
	}
	
	private BankCustomer customer;
	private String accountNumber;
	protected DollarAmount balance;
	
	
public BankCustomer getCustomer(){
	return customer;
}
public DollarAmount getBalance(){
	return balance;
}
	
public DollarAmount deposit(DollarAmount deposit){
	balance = balance.plus(deposit);
	return balance;
}
public DollarAmount withdraw(DollarAmount withdraw){
	balance = balance.minus(withdraw);
	return balance;
	
}
public void transfer(BankAccount destinationAccount, DollarAmount transferAmount){
	balance = balance.minus(transferAmount);
	destinationAccount.balance = (destinationAccount.balance).plus(transferAmount);
	
	
}

	
	
	
	
	
	
	
	

}
