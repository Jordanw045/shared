package com.techelevator;

import  org.junit.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class BankAccountTest {
	private BankAccount myAccount;
	
	@Before
	public void setup(){
		BankCustomer josh = new BankCustomer("josh", "123 lane", "6154");
		myAccount = new BankAccount(new DollarAmount(100), "563", josh);
	}
	
	@Test 
	public void get_customer (){
		myAccount.getCustomer();
	}
	 
	
	@Test 
	public void get_balance(){
		DollarAmount balance = myAccount.getBalance();
		
		Assert.assertEquals(balance.getDollars(), 1);
	}
	
	@Test
	public void deposit() {
		myAccount.deposit(new DollarAmount(200));
		Assert.assertEquals((myAccount.getBalance()).getDollars(), 3);
	}
	@Test
	public void withdraw (){
		myAccount.withdraw(new DollarAmount(100));
		Assert.assertEquals((myAccount.getBalance()).getDollars(),0);
	}
	@Test 
	public void transfer (){
		BankCustomer jane = new BankCustomer("josh", "123 lane", "6154");
		BankAccount secondBank = new BankAccount (new DollarAmount (100), "ufyb", jane);
		myAccount.transfer(secondBank, new DollarAmount (100));
		Assert.assertEquals((myAccount.getBalance()).getDollars(),0);
	}
	@Test 
	public void transfer_their_account(){
		BankCustomer jane = new BankCustomer("josh", "123 lane", "6154");
		BankAccount secondBank = new BankAccount (new DollarAmount (100), "ufyb", jane );
		myAccount.transfer(secondBank, new DollarAmount (100));
		Assert.assertEquals((secondBank.getBalance()).getDollars(),2);
	}

}
