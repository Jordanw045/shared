package com.techelevator;

import org.junit.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



public class DollarAmountTest {
private DollarAmount familyDollar;

@Before 
public void setup (){
	familyDollar = new DollarAmount (2300);


	}
@Test
public void intialize_core_values (){
	int wealth = familyDollar.getDollars();
	
	
	Assert.assertEquals(wealth, 23);
}

@Test
public void get_cents (){
	familyDollar.getCents();
	
	
}
@Test 
public void is_greater_than (){
	
familyDollar.isGreaterThan(familyDollar);

}

@Test 
public void great_or_equal (){
	familyDollar.isGreaterThanOrEqualTo(familyDollar);
}

@Test
public void is_less_than() {
	familyDollar.isLessThan(familyDollar);
}
@Test
public void is_less_equal(){
	familyDollar.isLessThanOrEqualTo(familyDollar);
}
@Test
public void is_less_than_False(){
	DollarAmount myDollar = new DollarAmount (1200);
	boolean less =familyDollar.isLessThanOrEqualTo(myDollar);
	
	Assert.assertFalse(less);
	
}
@Test
public void is_negative(){
	DollarAmount negDollar = new DollarAmount (-10);
	boolean neg = negDollar.isNegative();
	
	Assert.assertTrue(neg);
	
	
}
@Test
public void is_negative_false(){
	boolean neg =familyDollar.isNegative();
	Assert.assertFalse(neg);
	
	
}
@Test
public void minus (){
	familyDollar.minus(familyDollar);
}
@Test
public void plus (){
	familyDollar.plus(familyDollar);
}
@Test
public void compare_2_objects_same_value (){
	int compare =familyDollar.compareTo(familyDollar);
	
	Assert.assertEquals(compare,0);


}
@Test
public void compare_objects_great_value(){
	DollarAmount myDollar = new DollarAmount (2500);
	int compare =familyDollar.compareTo(myDollar);
	
	Assert.assertEquals(compare, -1);
	
	
}
@Test
public void compare_greater_equal_false (){
	DollarAmount myDollar = new DollarAmount (2500);
	boolean compare =familyDollar.isGreaterThanOrEqualTo(myDollar);
	
	Assert.assertFalse(compare);
}

@Test
public void compare_object_less_val(){
	DollarAmount yourDollar = new DollarAmount (1500);
	int compare =familyDollar.compareTo(yourDollar);
	
	
	Assert.assertEquals(compare,1);
}
@Test 
public void hash_code (){
	int compare = familyDollar.hashCode();
	
	Assert.assertEquals(compare, 2300);
}
@Test
public void object_equals_with_a_null_object (){
	String empty = null;
	boolean equals= familyDollar.equals(empty);
	
	Assert.assertFalse(equals);
}
@Test
public void object_equals_non_dollar_amount (){
	String wrong = "ubfr";
	boolean equals = familyDollar.equals(wrong);
	
	Assert.assertFalse(equals);
}
@Test
public void equals_with_dollar_amount (){
	DollarAmount zero = new DollarAmount(3);
	boolean equals = familyDollar.equals(zero);
	
	Assert.assertFalse(equals);
}

@Test
public void everything_is_finally_equal (){
	DollarAmount rival = new DollarAmount(2300);
	boolean equals = familyDollar.equals(rival);
	
	Assert.assertTrue(equals);
}
@Test
public void dollar_format_string (){
	String format = new DollarAmount(3210).toString();
	
	Assert.assertEquals(format,"$32.10");
	
}




}
