package com.techelevator;

import org.junit.Assert;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



public class BankCustomerTest {
	private BankCustomer john;
	private BankAccount myAccount;
	private BankCustomer joe;
	private BankAccount myAccount2;
	
	@Before 
	public void setup(){
		 BankAccount myAccount = new BankAccount(new DollarAmount(100), "563", john);
			john = new BankCustomer("John","5882 Tartob Circle", "615264825"); 
			 BankAccount myAccount2 = new BankAccount(new DollarAmount(350000), "563", joe);
			 joe = new BankCustomer("John","5882 Tartob Circle", "615264825");
			 
	}
	@Test
	public void initialize_values (){
		String name = john.getName();
		String address = john.getAddress();
		String phone = john.getPhoneNumber();
		
		
		
		Assert.assertEquals(name, "John");
		Assert.assertEquals(address, "5882 Tartob Circle");
		Assert.assertEquals(phone, "615264825");
		
		
	}
	@Test
	public void vip_test(){
		 boolean boo = john.isVIP();
		 boolean boo2 = joe.isVIP();
		 
		 Assert.assertFalse("Should be false",boo);
		 Assert.assertTrue("Should be true",boo2);
	
		
		
	}


}
