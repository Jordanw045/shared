package com.techelevator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CheckingAccountTest {
	private CheckingAccount myCheck;
	
	@Before 
	public void setup (){
		myCheck = new CheckingAccount(new DollarAmount(1000), "563", "Yolanda");
		
	}
	
	@Test 
	public void withdraw_no_penalty (){
		myCheck.withdraw(new DollarAmount(1000));
		Assert.assertEquals((myCheck.getBalance()).getDollars(),0);
		
	}
	@Test 
	public void withdraw_penalty (){
		myCheck.withdraw(new DollarAmount(1900));
		Assert.assertEquals(-9, (myCheck.balance).getDollars());
		
	}
	@Test
	public void withdraw_below_negative_onehundred (){
		myCheck.withdraw(new DollarAmount(100000000));
		Assert.assertEquals((myCheck.getBalance()).getDollars(),10);
		
	}

}
