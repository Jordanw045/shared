package com.techelevator;

public class ExFed implements DELIVERYDRIVERINTERFACE{
	protected double weight;
	protected boolean isOunces;
	protected int distance;


	public ExFed(double weight, boolean isOunces, int distance) {
	    
	    this.distance = distance;
	    this.isOunces = isOunces;
	    this.weight = weight;
	    
	}

	protected int getDistance(){
	    return distance;
	}
	protected double getWeight(){
	    return weight;
	}
	protected boolean isOunces(){
	    return isOunces;
	}

	@Override
	public String[] getRate() {
	    
	    double rate = 20.00;
	    
	    if(distance > 500){
	        rate += 5.00;
	    }
	    if((isOunces && weight > 48) || (!isOunces && weight > 3*16)){
	        rate += 3.00;
	    }
	    
	    String[] rates = new String[] {String.format("%-20s %-25s"+"$ "+"%.2f","ExFed","", rate) };
	    
	    return rates;
	}
	
}
