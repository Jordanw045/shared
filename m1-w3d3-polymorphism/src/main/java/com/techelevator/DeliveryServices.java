package com.techelevator;

import java.awt.List;
import java.util.ArrayList;
import java.util.Scanner;

public class DeliveryServices {
	public static void main(String[] args){
        
	    boolean bool = true;
	    ArrayList<String[]> deliveryList = new ArrayList<String[]>();
	    
	    Scanner in = new Scanner(System.in);
	    
	    System.out.println("Please enter the weight of the package? ");
	    double weight = in.nextDouble();
	    
	    System.out.println("(P)ounds or (O)unces? ");
	    String ozOrlb = in.next(); 
	    
	    System.out.println("What distance will it be traveling to? ");
	    int distance = in.nextInt();
	    in.close();
	    
	    if(ozOrlb.equals("P")) {

	        bool = false;
	    }
	    
	    deliveryList.add(new FexEd(weight, bool, distance).getRate());
	    deliveryList.add(new ExFed(weight, bool, distance).getRate());
	    deliveryList.add(new SPU(weight, bool, distance).getRate());
	        
	    System.out.printf("%-20s %31s \n","Delivery Method", "$ Cost");
	    System.out.println("=====================================================");
	    
	    for(String[] rates: deliveryList){
	        
	        for(String item: rates){
	            
	            System.out.println(item);
	        }
	    }
	}
}
