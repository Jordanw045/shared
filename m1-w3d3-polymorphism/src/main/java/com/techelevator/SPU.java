package com.techelevator;

public class SPU extends ExFed implements DELIVERYDRIVERINTERFACE {

private double weight;
private boolean isOunces;
private int distance;


public SPU(double weight, boolean isOunces, int distance) {
    super(weight, isOunces, distance);
    
}




@Override
public String[] getRate() {
    
    if(!isOunces){
        weight = weight*16;
    }
    String[] rates = new String[3];
    for(int i=0; i<3; i++){
        
        double rate;
        String tag;
        
        if(i == 0){
        
            rate = (this.getWeight() * 0.0050) * this.getDistance();
            tag = "(4-day ground)         ";
        }
        else if (i == 1){
        
            rate = (this.getWeight() * 0.050) * this.getDistance();
            tag = "(2-day business)       ";

        }
        else{
        
            rate = (this.getWeight() * 0.075) * this.getDistance();
            tag = "(next-day)             ";
        }
                    
        String outString = String.format("%-20s %-25s"+"$ "+"%.2f","SPU", tag, rate);
        rates[i] = outString;
    
    }
    return rates;
}
}


