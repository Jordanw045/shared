package com.techelevator;

public class FexEd extends ExFed implements DELIVERYDRIVERINTERFACE {
	private double weight;
	private boolean isOunces;
	private int distance;

	public FexEd(double weight, boolean isOunces, int distance) {
	    super(weight, isOunces, distance);
	        
	}

	private String[] calcOuncesRate(){
	    
	    String[] tags = new String[]{ "(1st Class)","(2nd Class)","(3rd Class)"};

	    double[][] rateTable = new double[][]{
	        
	        {0.035   ,   0.0035  ,   0.0020},
	        {0.040   ,   0.0040  ,   0.0022},
	        {0.047   ,   0.0047  ,   0.0024}
	    };
	    int row;    
	    if(this.getWeight() < 3){
	        row = 0;        
	    }
	    else if(this.getWeight() < 9){
	        row = 1;            
	    }
	    else{
	        row = 2;            
	    }
	    
	    String[] rates = new String[3];
	    for(int i=0; i<3;i++){
	                
	        double rate = this.getDistance()* rateTable[row][i];    

	        String outString = String.format("%-20s %-25s"+"$ "+"%.2f","Postal Service", tags[i], rate);            
	        rates[i] = outString;       
	    }
	    
	    return rates;
	}



	private String[] calcPoundsRate(){
	    
	    String[] tags = new String[]{ "(1st Class)","(2nd Class)","(3rd Class)"};
	            
	    double[][] rateTable = new double[][]{
	        
	        {0.195    ,   0.0195  ,   0.0150},
	        {0.450    ,   0.0450  ,   0.0160},
	        {00.500   ,   0.0500  ,   0.0170}
	    };
	    
	    int row;
	    if(this.getWeight() < 3){
	        row = 0;                
	    }
	    else if(this.getWeight() < 9){
	        row = 1;            
	    }
	    else{
	        row = 2;            
	    }
	    
	    String[] rates = new String[3];
	    for(int i=0; i<3;i++){
	            
	        double rate = this.getDistance() * rateTable[row][i];
	        String outString = String.format("%-20s %-25s"+"$ "+"%.2f","Postal Service", tags[i], rate);
	        rates[i] = outString;       
	    }       
	    return rates;
	}

	@Override
	public String[] getRate() {
	    
	    if(this.isOunces()){
	        
	        return this.calcOuncesRate();
	    }
	    else{
	        return this.calcPoundsRate();
	    }
	}
	
}
