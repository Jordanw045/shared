package techelevator;

import java.util.ArrayList;
import java.util.List;

public class Course {
	private String name;
	private String courseId;
	private String department;
	private String description;
	private int creditHour;
	private String teacher;
	private boolean isYearLong;
	private List<Date> schedule;
	private boolean hasLab;
	private List <Course> preRegs;
	private List<String> books;
	private List<Assignment> assignments;

	puclic Course(String name,String courseId,String department,int creditHour, boolean yearLong){
		this.name = name;
		this.courseId= courseId;
		this.department = department;
		this.creditHour = creditHour;
		this.isYearLong= yearLong;
		
		description = "No Description Yet";
		teacher = "Staff";
		schedule = new ArrayList <Date>();
		hasLab = false;
		preRegs = new ArrayList<Course>();
		books = new ArrayList<String>();
		assignments = new ArrayList<Assignment>();
		
		
		
	}
	public String getName(){
		return name;
	}
	public String getCourseId(){
		return courseId;
	}
	public String getDeparment (){
		return department;
	}
	public int getCreditHour (){
		return creditHour;
	}
	

}
