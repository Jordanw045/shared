package techelevator;

import java.util.List;

public class Student {
	private String name;
	private String studentId;
	private String email;
	private String major;
	private int gradeLevel;
	private List<Course> courses;
	
	public Student(String name, String studentId, String email) {
		this.name = name;
		this.studentId= studentId;
		this.email = email;
		
		major = "Undeclared";
		gradeLevel= 0;
		courses = new ArrayList<Course> ();
	}
	
	public String getName(){
		return name;
	}
	public String getStudentId(){
		return studentId;
	}
	public String getEmail(){
		return email;
	}
	public void setMajor(String major){
		this.major = major;
	}
	public String getMajor(){
		return major;
	}
	public void setGradeLevel(int gradeLevel){
		this.gradeLevel = gradeLevel;
	}
	public int getGradeLevel (){
		return gradeLevel;
		
	}
	
	public void addCourse(Course course) {
		courses.add(course);
	}
	public void dropCourses (){
		courses.remove(course);
	}
	public List<Course> getCourses(){
		return courses;
	}

}
