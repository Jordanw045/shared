package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataPrimeFactorsTest {
	private KataPrimeFactors factors;
	
	@Before 
	public void setup (){
		factors = new KataPrimeFactors();
	}
	@Test
	public void given_two_return_2 (){
		//arrange
		//act
		int[] results = factors.factorize(2);
		
		//Assert 
		Assert.assertEquals(1, results.length);
		Assert.assertEquals(2, results[0]);
		
	}
	@Test 
	public void given_three_return_three (){
		
		
		int results[] = factors.factorize(3);
		Assert.assertEquals(1, results.length);
		Assert.assertEquals(3, results[0]);
		
	}
	
	@Test
	public void given_four_return_two (){
		// Act
		int[] results= factors.factorize(4);
		
		//Assert
		Assert.assertEquals(2, results.length);
		Assert.assertEquals(2, results [0]);
		Assert.assertEquals(2, results [1]);
	}
	@Test
	public void given_six_return_two_three(){
		//Act
		int [] results =factors.factorize(6);
		
		Assert.assertEquals(2, results.length);
		Assert.assertEquals(2, results [0]);
		Assert.assertEquals(3, results [1]);
	}
	@Test
	public void given_seven_return_seven(){
		//Act
		int [] results =factors.factorize(7);
		
		Assert.assertEquals(1, results.length);
		Assert.assertEquals(7, results [0]);
		
	}
	@Test
	public void given_eight_return_two_two(){
		//Act
		int [] results =factors.factorize(8);
		
		Assert.assertEquals(3, results.length);
		Assert.assertEquals(2, results [0]);
		Assert.assertEquals(2, results [1]);
		Assert.assertEquals(2, results [2]);
		
	}
	@Test
	public void given_nine_return_three_three(){
		int [] results = factors.factorize(9);
		Assert.assertEquals(2, results.length);
		Assert.assertEquals(3, results [0]);
		Assert.assertEquals(3, results [1]);
	}
	@Test
	public void given_ten_return_two_five (){
		int [] results = factors.factorize(10);
		Assert.assertEquals(2, results.length);
		Assert.assertEquals(2, results [0]);
		Assert.assertEquals(5, results [1]);
		
	}
	@Test 
	public void given_negative_10_return_empty_array(){
		int [] results = factors.factorize(-10);
				
		Assert.assertEquals(0, results.length);		
	}
	
	
	

}
