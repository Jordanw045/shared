package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataFizzBuzzTest {
	private KataFizzBuzz boss;
//	1 ->  returns "1"
//	2 -> returns "2"
//	3 -> returns "Fizz"
//	4 -> returns "4"
//	5 -> returns "Buzz" 
//	15 -> returns "FizzBuzz"
//	... etc up to 100
	
	@Before
	public void setup() {
		boss= new KataFizzBuzz();
		
	}
	@Test
	public void return_not_FizzBuzz(){
		String num = KataFizzBuzz.convert(203);
		
		Assert.assertEquals("203", num);
	}
	@Test
	public void return_buzz(){
		String num = KataFizzBuzz.convert(10);
		
		Assert.assertEquals("Buzz", num);
	}
	@Test
	public void return_fizz(){
		String num = KataFizzBuzz.convert(12);
		
		Assert.assertEquals("Fizz", num);
	}
	@Test
	public void return_fizz_buzz(){
		String num = KataFizzBuzz.convert(15);
		
		Assert.assertEquals("FizzBuzz", num);
	}
	
	

}
