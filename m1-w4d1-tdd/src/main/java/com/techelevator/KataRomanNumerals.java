package com.techelevator;

import java.util.TreeMap;

import javax.swing.text.StringContent;

public class KataRomanNumerals {
	private final static TreeMap<Integer, String> map = new TreeMap<Integer, String>();

    static {

        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");

    }

    public static String toRoman(int number) {
        int l =  map.floorKey(number);
        if ( number == l ) {
            return map.get(number);
        }
        return map.get(l) + toRoman(number-l);
    }
    public void testRomanConversion() {

        for (int i = 1; i<= 100; i++) {
            System.out.println(i+"\t =\t "+ KataRomanNumerals.toRoman(i));
        }

    }
    public static int toInt(String number) {
        if (number == null) return 0;
        if (number.startsWith("M")) return 1000;
        if (number.startsWith("CM")) return 900;
        if (number.startsWith("D")) return 500 ;
        if (number.startsWith("CD")) return 400 ;
        if (number.startsWith("C")) return 100;
        if (number.startsWith("XC")) return 90;
        if (number.startsWith("L")) return 50;
        if (number.startsWith("XL")) return 40;
        if (number.startsWith("X")) return 10;
        if (number.startsWith("IX")) return 9;
        if (number.startsWith("V")) return 5 ;
        if (number.startsWith("IV")) return 4 ;
        if (number.startsWith("I")) return 1 ;
        throw new Exception("something bad happened");
    }
	
	
	

}
