package com.techelevator;

import java.util.ArrayList;
import java.util.List;

public class KataFizzBuzz {
	
	public static String convert(Integer i){
		if (i % 3 == 0 && i % 5 == 0){
			return "FizzBuzz";
		}else if (i % 3 == 0){
			return "Fizz";
		}else if (i % 5 == 0) {
			return "Buzz";
		}
		return Integer.toString(i);
	}
		
	

}
